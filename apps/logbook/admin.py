from django.contrib import admin

from .models import Logbook, Event


class LogbookAdmin(admin.ModelAdmin):
    list_display = ('employee_id', 'start_date', 'end_date', 'hours')
    search_fields = ('employee_id',)
    readonly_fields = list_display

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class EventAdmin(admin.ModelAdmin):
    list_display = ('employee_id', 'direct', 'timestamp', 'datetime')
    readonly_fields = list_display

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(Logbook, LogbookAdmin)
admin.site.register(Event, EventAdmin)
