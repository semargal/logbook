from django.core.urlresolvers import reverse
from django.test import TestCase

from logbook.models import Event, Logbook


class LogbookTest(TestCase):

    def _post_enter(self):
        resp = self.client.post(reverse('logbook:update'), {
            'employee_id': '132456789',
            'direct': 'in',
            'timestamp': '1449659143.0'
        })
        return resp

    def _post_exit(self):
        resp = self.client.post(reverse('logbook:update'), {
            'employee_id': '132456789',
            'direct': 'out',
            'timestamp': '1449659144.0'
        })
        return resp

    def test_user_in(self):
        """Feature: An employee entered"""

        self.assertEqual(self._post_enter().content, '{"status": "ok"}')

        self.assertEqual(Event.objects.count(), 1)
        self.assertEqual(Logbook.objects.count(), 0)

    def test_user_out(self):
        """Feature: An employee exited"""
        self._post_enter()
        resp = self._post_exit()
        self.assertEqual(resp.content, '{"status": "ok"}')

        self.assertEqual(Event.objects.count(), 2)
        # One entry per in/out.
        self.assertEqual(Logbook.objects.count(), 1)

    def test_user_in_unusual(self):
        """Feature: An employee is trying to enter but he has already entered"""
        self._post_enter()
        resp = self._post_enter()
        self.assertContains(resp, 'error')

        self.assertEqual(Event.objects.count(), 1)
        self.assertEqual(Logbook.objects.count(), 0)

    def test_user_out_unusual_case1(self):
        """Feature: An employee is trying to exit but he has already exited"""
        self._post_enter()
        self._post_exit()
        resp = self._post_exit()
        self.assertContains(resp, 'error')

    def test_user_out_unusual_case2(self):
        """Feature: An employee can\'t exit if he has not entered"""
        resp = self._post_exit()
        self.assertContains(resp, 'error')
