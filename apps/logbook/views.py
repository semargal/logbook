from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .models import Event
from .forms import EventForm


# TODO: check permissions
@csrf_exempt
def update(request):
    msg = {'status': 'ok'}

    form = EventForm(request.POST or None)
    if form.is_valid():
        try:
            Event.objects.log_event(**form.cleaned_data)
        except Event.EventLogError as e:
            msg.update({
                'status': 'error',
                'errors': str(e)
            })
        else:
            return JsonResponse(msg)

    else:
        msg.update({
            'status': 'error',
            'errors': form.errors.as_json()
        })
    return JsonResponse(msg)
