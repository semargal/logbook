from __future__ import unicode_literals
from datetime import datetime
from decimal import Decimal

from django.db import models, transaction
from django.conf import settings
from django.utils.timezone import make_aware
from django.utils.translation import ugettext_lazy as _


class EventManger(models.Manager):

    def log_event(self, **data):
        data['direct'] = Event.clean_direct(data['direct'])

        if settings.USE_TZ:
            dt = datetime.utcfromtimestamp(float(data['timestamp']))
            dt = make_aware(dt)
        else:
            dt = datetime.fromtimestamp(float(data['timestamp']))
        data['datetime'] = dt
        data['timestamp'] = Decimal(str(data['timestamp']))

        event = Event(**data)

        if event.is_outside():
            start_event = self.filter(
                employee_id=event.employee_id, direct=Event.IN).first()

            try:
                with transaction.atomic():
                    event.save()
                    hours = (
                        event.timestamp - start_event.timestamp) / Decimal(3600)
                    try:
                        logbook = Logbook.objects.get(
                            employee_id=event.employee_id,
                            start_date=start_event.datetime.date(),
                        )
                        logbook.hours += hours
                        logbook.end_date = event.datetime.date()
                        logbook.save()
                    except Logbook.DoesNotExist:
                        Logbook.objects.create(
                            employee_id=event.employee_id,
                            start_date=start_event.datetime.date(),
                            end_date=event.datetime.date(),
                            hours=hours,
                        )
            except Exception as e:
                # TODO: log exception.
                raise Event.EventLogError(_('An unhandled error has occured'))
        else:
            event.save()


class Event(models.Model):
    """
    Log of employees events.
    """
    IN = 1
    OUT = 0
    DIRECT_CHOICES = (
        (IN, _('in')),
        (OUT, _('out')),
    )
    DIRECT_MAP = {
        'in': IN,
        'out': OUT
    }

    employee_id = models.PositiveIntegerField(_('employee ID'), db_index=True)
    direct = models.PositiveSmallIntegerField(
        _('direct'), choices=DIRECT_CHOICES, default=0)
    timestamp = models.DecimalField(
        _('timestamp'), max_digits=16, decimal_places=6)
    datetime = models.DateTimeField(_('date/time'))

    objects = EventManger()

    class Meta:
        verbose_name = _('event')
        verbose_name_plural = _('events')
        ordering = ('-pk',)

    class EventLogError(Exception):
        pass

    def __unicode__(self):
        return unicode(self.employee_id)

    def is_outside(self):
        return self.direct == self.OUT

    def is_inside(self):
        return self.direct == self.IN

    @classmethod
    def clean_direct(cls, direct):
        if isinstance(direct, basestring):
            direct = cls.DIRECT_MAP[direct]
        return direct


class Logbook(models.Model):
    """
    Logbook of employees' time.
    """
    employee_id = models.PositiveIntegerField(_('employee ID'), db_index=True)
    start_date = models.DateField(_('start date'), db_index=True)
    end_date = models.DateField(_('start date'), db_index=True)
    hours = models.DecimalField(
        _('time'), max_digits=4, decimal_places=2, default=0)

    class Meta:
        verbose_name = _('logbook')
        verbose_name_plural = _('logbooks')
        unique_together = ('employee_id', 'start_date', 'end_date')
        ordering = ('-pk',)

    def __unicode__(self):
        return unicode(self.employee_id)
