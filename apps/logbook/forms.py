# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _

from .models import Event


class EventForm(forms.Form):
    employee_id = forms.IntegerField(min_value=1)
    direct = forms.CharField()
    timestamp = forms.FloatField()

    def clean_direct(self):
        direct = self.cleaned_data['direct']
        if not direct in ('in', 'out'):
            raise forms.ValidationError(
                _('Value must be "in" or "out."'), code='invalid')
        return direct

    def clean(self):
        data = self.cleaned_data

        employee_id = data.get('employee_id')
        direct = data.get('direct')

        if employee_id and direct:
            last_event = Event.objects.filter(employee_id=employee_id).first()
            direct = Event.clean_direct(direct)
            if last_event:
                if last_event.is_inside() and direct == Event.IN:
                    self.add_error(
                        'direct', _('An employee can\'t enter twice.'))
                elif last_event.is_outside() and direct == Event.OUT:
                    self.add_error(
                        'direct', _('An employee can\'t exit twice.'))
            if direct == Event.OUT and (
                    not last_event or last_event.is_outside()):
                self.add_error(
                    'direct',
                    _('An employee can\'t exit if he has not entered.'))
        return data

